from collections import Counter
import numpy as np
import math

# Cria os 10 mil números randomicos entre 1 e 2 milhões
numbers = np.random.randint(2000000, size=10000)

# Função que extrai o primeiro dígito
# (baixa performance)first_digit = lambda num:num // 10 ** (int(math.log(num, 10)))
first_digit = lambda number:int(str(number)[:1])

# Define a função de vetorização
vectorizeFunc = np.vectorize(first_digit)
# Aplica em numbers e retorna um np.array com os primeiros dígitos deles
firstDigitsOfNumbers = vectorizeFunc(numbers)

# Usa o Counter para agrupar os firstDigitsOfNumbers
counter = Counter(firstDigitsOfNumbers).most_common()

# Ordena o counter de 1 a 9
sortedCounter = sorted(counter)

# Printa a lista de (numero, quantidade)
print(sortedCounter)